// lib imports
const router = require('express').Router();
const winston = require('winston');

// app imports
const health = require('./health');

router.use((req, res, next) => {
    winston.log('info', `METHOD: ${req.method} URL: ${req.url}`);
    next();
});

router.use('/health', health);

module.exports = router;