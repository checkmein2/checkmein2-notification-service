// lib imports
const fs = require('fs');

const version = JSON.parse(fs.readFileSync('package.json')).version;

const health = (req, res, next) => {
    res.status(200).send(version);
    next();
};

module.exports = health;