// lib imports
const express = require('express');
const helmet = require('helmet');
const winston = require('winston');
const swaggerTools = require('swagger-tools');

// app imports
const router = require('../api');
const swaggerDoc = require('../docs/swagger.json');

const port = process.env.PORT || 1111;
const app = express();

app.use(helmet());
app.use(router);

//Swagger setup
if (process.env.NODE_ENV === "development") {
    swaggerTools.initializeMiddleware(swaggerDoc, (middleware) => {
    // Serve the Swagger documents and Swagger UI
    app.use(middleware.swaggerUi());
    });
}

const startApp = () => app.listen(port, (err) => {
    if (err) {
        winston.log('info', 'APP CAN NOT BE STARTED');
        winston.log('error', err);
    }

    winston.log('info', `APP STARED AT PORT ${port}`);
});

module.exports = {
    startApp
};
