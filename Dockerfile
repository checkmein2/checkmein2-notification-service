FROM node:7.7.3-alpine
# Create app directory
RUN mkdir -p /app
WORKDIR /app
# Install app dependencies
COPY package.json /app/
# Bundle app source
RUN yarn
ENV PORT 8080
COPY . /app
EXPOSE 8080
CMD [ "yarn", "start" ]